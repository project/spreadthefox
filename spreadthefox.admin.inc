<?php

function spreadthefox_page () {

    drupal_add_css (drupal_get_path('module', 'spreadthefox') . '/css/sfx_style.css');
    drupal_add_js (drupal_get_path('module', 'spreadthefox') . '/js/sfx.js');
    drupal_add_js (drupal_get_path('module', 'spreadthefox') . '/js/jquery.selectboxes.min.js');

    $output = drupal_get_form('spreadthefox_sfxform');

    return $output;
}

function spreadthefox_sfxform () {

    $strAffiliateId = variable_get('spreadthefox_affiliate_id', null);

    $form['spreadthefox_affiliate_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Affiliate ID'),
        '#default_value' => $strAffiliateId,
        '#maxlength' => 64,
        '#description' => t('Note: Affiliate ID can be found by logging into your SFx account and clicking on My Account Info.'),
        '#required' => false,
        '#weight' => -19
    );
    

    $form['spreadthefox_thickbox'] = array(
        '#value' => spreadthefox_get_thickbox()
    );

    $form['spreadthefox_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );

    return $form;
}

// NOTES: Any validations we need to do? Check if affiliate id is an int?
function spreadthefox_sfxform_validate ($form, &$form_state) {}

function spreadthefox_sfxform_submit($form, &$form_state) {
	
	// Grabbing from post, bad?
    $strAffiliateId = $form_state['values']['spreadthefox_affiliate_id'];
    $strLocale = $form['#post']['sfx_locale'];
    $strButtonSize = $form['#post']['sfx_button_size'];
    $strButtonListArray = $form['#post']['sfx_button_list'];
    
    variable_set('spreadthefox_affiliate_id' , $strAffiliateId);
    variable_set('spreadthefox_locale', $strLocale);
    variable_set('spreadthefox_button_size', $strButtonSize);
    variable_set('spreadthefox_button_array', $strButtonListArray);

    drupal_set_message( t('Options updated.') );

}

function spreadthefox_get_thickbox() {	

	global $base_url;

    $strToReturn = sprintf ('
        <label class="sfx_label"><b>Selected Locale</b>: <span id="sfx_lbl_locale">%s</span></label>
        <label class="sfx_label"><b>Selected Size</b>: <span id="sfx_lbl_size">%s</span></label>
        <a href="#TB_inline?height=550&width=520&inlineId=sfx_option_window" title="" class="thickbox" onclick="sfx_update_buttons(\'%s\',\'%s\', \'%s\');">Edit Image Options</a>
        <p/>
        %s',
        variable_get('spreadthefox_locale', 'Not Selected'),
        variable_get('spreadthefox_button_size', 'Not Selected'),
        variable_get('spreadthefox_locale', null),
        variable_get('spreadthefox_button_size', null),
		$base_url . '/' . drupal_get_path('module', 'spreadthefox'),
        sfx_control_thickbox()
    );

    return $strToReturn;

}

function sfx_control_thickbox() {

     $strLocale = variable_get('spreadthefox_locale', null);
     $strButtonSize = variable_get('spreadthefox_button_size', null);
     $strButtonListArray = variable_get('spreadthefox_button_array', null);

     return sprintf ('
        <div id="sfx_option_window">
            <img class="sfx_ff_logo" src="%s" alt="Firefox Logo" />
            <h3 class="sfx_image_options" >Spread Firefox: Image options</h3>
            <br style="clear:both;"/>
            <label class="sfx_label">Locale:
                <select id="sfx_locale" name="sfx_locale" class="form-select" onchange="sfx_update_images();">
                    %s
                </select>
            </label>
            <label class="sfx_label">Size:
                <select id="sfx_button_size" name="sfx_button_size" class="form-select" onchange="sfx_update_images();">
                    %s
                </select>
            </label>
            <input type="hidden" id="sfx_button_list" name="sfx_button_list" value="%s" />
            <p/><a class="sfx-form-submit" href="#save:spread-firefox" style="float:none;" onclick="sfx_update_labels();">Done</a>
            <p/>Preview:
            <div id="sfx_image_preview"></div>
        </div>',
        '/' . drupal_get_path('module', 'spreadthefox') . '/images/ff_wordmark.png',
        ($strLocale) ? '<option value="' . $strLocale . '">' . $strLocale . '</option>' : '',
        ($strButtonSize) ? '<option value="' . $strButtonSize . '">'.$strButtonSize.'</option>' : '',
        ($strButtonListArray) ? $strButtonListArray : ''

    );
}

?>
