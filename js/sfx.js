// global variable for selected buttons
var jsonButtons;

/**
 *  Animates the header drop down for IE6 users
 *  return;
**/
function sfx_animate_dropdown () {
    jQuery.each(jQuery.browser, function(i, val) {
        if ( (i== 'msie' && val) && (parseFloat(jQuery.browser.version) <= 6.0) ) {
            if (!sfx_check_cookie()) {
                jQuery("div#sfx_header").slideDown(1000);
                jQuery("div#sfx_close").slideDown(1000);
                sfx_cookie();
            }
        }
    });
}

/**
 *  Based on the current user browser, applys the administrators selected image to the page
 *  @string strSize The selected size from administrative options
 *	@string strLocale The selected locale from administrative options
 *	@string strAffiliateId The entered affiliate id from administrative options
 *	@string	strButtonList A string consisted of images from the administrative options
 *	@return;
**/
function sfx_apply_image (strSize, strLocale, strAffiliateId, strButtonList) {

    var strCurrentBrowser = 'other';

    // detect browser
    if (jQuery.browser.msie)
        strCurrentBrowser = 'msie';

    if (jQuery.browser.mozilla) {
        strCurrentBrowser = 'firefox';
        if (jQuery.browser.version.substr(0,3) > parseFloat("1.9") )
            strCurrentBrowser = 'firefox35';
    }

    var imageList = strButtonList.split('::');
    jQuery.each (imageList, 
    	function (i, imageData) {
		var strDataArray = imageData.split('||');
		if (strDataArray) {		
			if (strCurrentBrowser == strDataArray[0].toLowerCase() ) {
				jQuery("#sfx_image").attr("src", strDataArray[1]);	
				strDataArray[2] = strDataArray[2].replace(/&amp;/g,'&');
				strDataArray[2] =  strDataArray[2].replace(/(&|\?)uid=\d+/, '');
				 if (strAffiliateId) {
				     if (strDataArray[2].indexOf('?') == -1) {
					 jQuery("#sfx_image_url").attr("href", strDataArray[2] + '?uid=' + strAffiliateId);
				     } else {					 
					 jQuery("#sfx_image_url").attr("href", strDataArray[2]+ '&uid=' + strAffiliateId);
				     }
				 } else {
				    jQuery("#sfx_image_url").attr("href", strDataArray[2]);
				 }
			}		
		}		
	}
    );

}

/**
 *  Set a cookie for IE6 users letting use know they have viewed the drop down
 *	@return;
**/
function sfx_cookie () {
    var date = new Date();
    date.setTime(date.getTime() + (7*24*60*60*60) );
    document.cookie = "msie6message=yes; expires=" + date.toGMTString() + "; path=/";
}

/**
 *  Check to see if we have a cookie set
 *	@return true: true, false: null
**/
function sfx_check_cookie () {
    var results = document.cookie.match ( '(^|;)?msie6message' );
    if (results) return true; else return null;
}

/**
 *  Update the administrative buttons on the administrative side.  Call local json data
 *	and remote json data and merge the two.
 *	@string strLocale The default locale variable
 *	@string strSize The default size variable
 *	@string strPluginUrl The URL path base to the modules directory
 *	@return;
**/
function sfx_update_buttons (strLocale, strSize, strPluginUrl) {

	jQuery.getJSON(strPluginUrl + "/js/sfx.json",
		function(localJson){
			jQuery.getJSON(strPluginUrl + "/getRemoteJson.php?r=" + Math.floor(Math.random()*9999999),
				function(data){
					jsonButtons = jQuery.extend(localJson,data);

					jQuery("#sfx_locale").removeOption(/./);
					jQuery("#sfx_button_size").removeOption(/./);
					jQuery.each(data.buttons,
						function(i,button){
						    // Add strSize data to select box
						    if (strSize.toLowerCase() == button.size.toLowerCase())
							jQuery("#sfx_button_size").addOption (button.size, button.size, true);
						    else
							jQuery("#sfx_button_size").addOption (button.size, button.size, false);

						    // Add strLocale data to select box
						    if (strLocale.toLowerCase() == button.locale.toLowerCase())
							jQuery("#sfx_locale").addOption (button.locale, button.locale, true);
						    else
							jQuery("#sfx_locale").addOption (button.locale, button.locale, false);

						    // Show the images matching selection, the preview box, uncomment to view example
						    if (strSize.toLowerCase() == button.size.toLowerCase() && strLocale.toLowerCase() == button.locale.toLowerCase())
						       jQuery("<img/>").attr("src", button.src).appendTo("#sfx_image_preview");

						}
					);
					jQuery("#sfx_locale").sortOptions();
					jQuery("#sfx_button_size").sortOptions();
					sfx_update_images();
				}
			);
		}
	);
}

/**
 *  Update the labels showing which options have been selected in the administrative section
 *	@return;
**/
function sfx_update_labels () {

     var strLocale = jQuery("#sfx_locale option:selected");
     var strSize = jQuery("#sfx_button_size option:selected");
	
	 if (strLocale) strLocale = strLocale[0].value;
	 if (strSize) strSize = strSize[0].value;

     jQuery("#sfx_lbl_size").text(strSize);
     jQuery("#sfx_lbl_locale").text(strLocale);

     tb_remove();

}

/**
 *  Update #sfx_image_preivew to show the current selected images in administrative back-end.
 *	@return;
**/
function sfx_update_images () {
    if (jsonButtons) {
        var strJsonButtons;
        var locale = jQuery("#sfx_locale option:selected")[0].value;
        var size = jQuery("#sfx_button_size option:selected")[0].value;
        jQuery("#sfx_image_preview").text('');
        jQuery.each (jsonButtons.buttons,
            function (i, button ) {
                if (size.toLowerCase() == button.size.toLowerCase() && locale.toLowerCase() == button.locale.toLowerCase()) {
                    // Show the images matching selection, uncomment to view example
                    jQuery("<img/>").attr("src", button.src).appendTo("#sfx_image_preview");

                    if (!strJsonButtons)
                        strJsonButtons = button.browser + '||' + button.src + '||' + button.url + '::';
                    else
                        strJsonButtons += button.browser + '||' + button.src + '||' + button.url + '::';

                }
            }
        );
        jQuery("#sfx_button_list").attr("value", strJsonButtons);
    }
}
